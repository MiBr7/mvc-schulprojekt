package model;

public class Namber {
    private final int UPPER_LIMIT = 256;
    private final int LOWER_LIMIT = 0;
    private int num;
    private APICall apiCall = new APICall("http://www.alphanudel.de:5001");


    public void increment() {
        this.num = (hasReachedUpperLimit()) ? num : this.num + 1;
        apiCall.setNumberCall(num);
    }

    public void decrement() {
        this.num = (hasReachedLowerLimit()) ? num : this.num - 1;
        apiCall.setNumberCall(num);
    }

    private boolean hasReachedUpperLimit() {
        return this.num + 1 > this.UPPER_LIMIT;
    }

    private boolean hasReachedLowerLimit() {
        return this.num - 1 < this.LOWER_LIMIT;
    }

    public int getNum() {
        this.num = apiCall.getNumber();
        return num;
    }


}
