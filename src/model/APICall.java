package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APICall {

    private String url;
    private int value;

    public APICall(String url) {
        this.url = url;
    }

    public int getNumber() {
        try {
            URL obj = new URL(this.url + "/number");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url + "/number");
            System.out.println("Response Code : " + responseCode);
            StringBuilder response = getResponse(con);

            System.out.println(response.toString());
            con.disconnect();
            value = Integer.parseInt(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return value;
    }

    public void setNumberCall(int i) {

        try {
            URL obj = new URL(this.url + "/number?value=" + i);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");


            con.setDoOutput(true);
            System.out.println("send");

            int responseCode = con.getResponseCode();
            System.out.println("\nSending request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            StringBuilder response = getResponse(con);

            System.out.println(response.toString());
            con.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private StringBuilder getResponse(HttpURLConnection con) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response;
    }
}
