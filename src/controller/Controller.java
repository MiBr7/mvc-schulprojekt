package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Namber;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller {

    @FXML
    GridPane grid;
    @FXML
    Button deButton;
    @FXML
    Button inButton;
    @FXML
    TextField textField;

    private Namber namber;
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);



    @FXML
    public void initialize() {
        this.namber = new Namber();
        this.textField.setText(String.valueOf(namber.getNum()));
        scheduler.scheduleAtFixedRate(() -> textField.setText(String.valueOf(namber.getNum())), 1, 1 ,TimeUnit.SECONDS);
    }

    public void handleDecrement() {
        namber.decrement();
        textField.setText(String.valueOf(namber.getNum()));
    }

    public void handleIncrement() {
        namber.increment();
        textField.setText(String.valueOf(namber.getNum()));
    }
}
