#!/usr/bin/env bash

docker run -it -d --restart always --name mysqlAPI -p 5001:5000 -v /home/git/mysqlAPI:/code python:3 /bin/sh -c "pip3 install --no-cache-dir -r /code/Server/requirements.txt && python /code/Server/app.py"
# docker run -it --rm --name mysqlAPI -p 5000:5000 -v /Users/tom/Development/mysqlAPI:/code python:3 /bin/sh -c "pip3 install --no-cache-dir -r /code/Server/requirements.txt && python /code/Server/app.py"