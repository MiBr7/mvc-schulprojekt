from flask import Flask, request
import pymysql.cursors


app = Flask(__name__)


@app.route('/number')
def number():
    conn = pymysql.connect(host='HOSTNAME', port=3306, user='USERNAME', passwd='PASSWORD', db='DBNAME')
    cur = conn.cursor()
    value = request.args.get('value')

    if value is not None:
        try:
            value = int(value)
            if 0 <= value <= 256:
                cur.execute("UPDATE number SET value=%d" % (value,))
                conn.commit()
                cur.execute("SELECT value FROM number")
                conn.commit()
                return str(cur.fetchone()[0])
            else:
                return "Value does no fit"
        except ValueError:
            return "Wrong Value input"
    else:
        cur.execute("SELECT value FROM number")
        conn.commit()
        return str(cur.fetchone()[0])

if __name__ == '__main__':
    app.run(host='::', port=5000)
