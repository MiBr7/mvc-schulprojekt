# Dokumentation MVC Pattern
## Inhaltsverzeichnis
* [Allgemeines über das MVC Pattern](#allgemeines-über-das-mvc-pattern)
* [Installationsanleitung](#installationsanleitung)
* [Klassendiagramm](#klassendiagramm)

## Allgemeines über das MVC Pattern
Model-View-Controller oder auch kurz MVC genannt, besteht aus drei Modulen: Dem Model, der View und dem Controller.

Das Model dient zur Speicherung von Daten und gewährt anderen Modulen den Zugriff auf die jeweiligen Zustandsdaten.

Die View dient zur Visualisierung der Daten der Models. Eine View ist nur einem Model zugeordnet,
allerdings kann ein Model mehrere Views besitzen.

Der Controller dient als Schnittstelle für die View und des Models. In dem Controller ist keine Logik vorhanden,
dieser dient nur als Vermittler zwischen der Interaktion des Users mit der View und dem Model.
Ein Controller kann für alle Views zuständig sein, es ist aber auch gängig das ein Controller nur für eine bestimmte View zuständig ist.

Dieses Pattern kann im Allgemeinen als Entwurfsmuster oder auch Architekturmuster eingesetzt werden. Ziel dieses Patterns ist es, das Programm so zu entwickeln, sodass es flexibler wird. Dies bedeutet, dass man Komponenten einfach austauschen kann und Komponenten auch wiederverwendbar sind.

Obwohl MVC schon etwas älter ist, findet es sehr häufig Anwendung bei Webanwendungen, da es dort schon vorab eine Trennung zwischen Client und Server gibt. Anhand von HTTP-Requests werden die Interaktionen des Users verarbeitet. Der Server enthält einen "Router", welcher den Request an den jeweiligen Controller weiterleitet. Für die Logik wird daraufhin ein Model verwendet, welches meistens mit einer Datenbank gekoppelt ist.

### Vorteile
MVC bietet einige Vorteile. Es ist eine klare Trennung zwischen der GUI und der Logik vorhanden. Die GUI sollte nämlich keine Logik enthalten, sondern die Zustände des Models nur darstellen. Das Model selbst entscheidet, anhand seiner Logik, welchen Zustand es zurückliefert.  
Ein weiterer wichtiger Vorteil ist die Flexibilität des MVC. Zum Beispiel, sobald man sich für ein neues GUI Framework entscheidet kann man dies ganz einfach austauschen. Desweiteren kann z.B. die Datenbank welche für die Speicherung des Models verwendet wird auch ganz simpel ausgetauscht werden.  
Ein Model kann zudem durch verschiedene Views dargestellt werden.

### Nachteile
Ein Nachteil ist, dass MVC für kleine Projekte weniger geeignet ist, da es einen gewissen Mehraufwand hat. Für unser Projekt z.B. war es etwas ungeeignet, man hat zwar eine klare Trennung, doch auch dafür mehr Code und Files.

Dies kann man als Vorteil und als Nachteil sehen, denn dieses Pattern verlangt je nach Größe des Projektes ein umfassendes Wissen von verschiedenen Technologien

## MVC anhand der JavaFx Anwendung
Ein einfaches MVC kann man anhand der JavaFx Anwendung erkennen. Hier wurde ein Controller, ein Model namens Namber und eine view namens sample.fxml, erstellt. Der Controller wartet bis ein Button vom User betätigt wird und leitet die Interaktion an das Model weiter. Dieses verarbeitet dann die jeweilige Interaktion und gibt den aktuellen Zustand zurück, welcher dann in der View dargestellt wird.

## MVC Web-Frameworks
Es gibt schon einige Frameworks welche nach dem MVC-Pattern aufgebaut sind. Beispielsweise Für Webanwendungen wären dies:
- Springboot (Java)
- Ruby on Rails (Ruby)
- Laravel
- ...

Mit diesen Frameworks muss man nicht ein Projekt von Scratch nach MVC aufbauen, dies erledigen die Frameworks für einen. Außerdem erleichtern diese Frameworks den Einstieg in MVC und man wird schon aufgrund deren Architektur damit vertraut gemacht.

## Installationsanleitung
Zur besseren Visualisierung des MVC Patterns haben wir eine kleinen Use Case entwickelt. Ziel war es mit unterschiedlichen Views auf ein und das selbe Modell zuzugreifen. In dem Modell selbst wird ein Datensatz persistiert und die unterscheidlichen Views können mit Hilfe deren Controller darauf zugreifen, den Datensatz lesen und anpassen.

### MySQL Datenbank
Eine MySQL Datenbank kann sehr leicht innerhalb einer Docker Instanz gehostet werden. Docker bietet den unglaublichen Vorteil sehr schnell und effizient einzelne Container skalieren zu können. Container können einzeln bereitgestellt, aufgerufen und aktualisiert werden.
Da wir mit dieser Technologie sehr vertraut sind und die Vorteile von Docker schätzen, haben wir auch in diesem Fall uns dafür entschieden darauf zurückzugreifen.
Mit folgendem Befehl auf dem Host Rechner lässt sich der Container starten, Vorraussetzung ist dabei, dass Docker auf dem Host Rechner installiert ist: `docker run -it -d --restart always --name mysql -p 3306:3306 -v /path/to/my/mysql/data/directory:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=MyPersonalPassword mysql:latest`

### Backend API
Die Schnittstelle zu der Datenbank läuft ebenfalls in einem Docker Container und kann mit Hilfe von dem Shell Script `api/start.sh` gestarten werden. Es handelt sich dabei um eine Python flask Anwendung, zu beachten ist dabei auf welchem Port diese Anwendung laust. Gegebenfalls muss dieser vorerst noch angepasst werden, aktuell läuft die Anwendung auf dem Port `5001` auf dem Host Rechner.

Gern kann auch die aktuell gehostete Instanz der Schnittstelle unter folgender URL benutzt werden: `http://alphanudel.de:5001/number`. Eine neue Eingabe speichern funktioniert zum Beispeil folgendermaßen: `http://alphanudel.de:5001/number?value=10`.
<br><img src="images/api_gui.png" width="300" />

### Java Anwendung
Hierfür kann dieses gesamte Repository gern zum Beispiel als ZIP Datei heruntergeladen werden.
In dem Pfad `artifacts/JavaFXApp` befindet sich ein Jar-File `JavaFXApp.jar`.
Dieses Jar-File wird mit dem Befehl `java -jar JavaFXApp.jar` ausgeführt oder alternativ mit einem einfachen Doppelklick auf die Datei.
<br><img src="images/Java_App_GUI.png" width="300" />

### Swift iPhone App
Für die Swift App wird sowohl Xcode, als auch ein iOS Gerät benötigt. Per Doppelklick kann das Projekt `iPhone App/MVC School Project.xcodeproj` geöffnet werden. In Xcode selbst kann dann ausgewählt werden, ob die Anwendung lokal mit einem iOS Simulator oder mit einem extern verbundetem Gerät ausgeführt werden soll.
<br><img src="images/iPhone_App_GUI.png" width="300" />

## Klassendiagramm
Das Klassendiagramm befindet sich im folgenden Pfad: `images/Klassendiagramm.png`
<br><img src="images/Klassendiagramm.png" width="300" />
