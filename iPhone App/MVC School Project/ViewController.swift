//
//  ViewController.swift
//  MVC School Project
//
//  Created by Tom Hüttmann on 20.11.17.
//  Copyright © 2017 Tom Hüttmann. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ModelDelegate {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    private let model = Model()
    var timer = Timer()
    
    var labelText: String {
        get {
            return label.text!
        }
        set {
            label.text = newValue
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.getCurrentNumber()
        scheduledTimerWithTimeInterval()
        
        stepper.frame = CGRect(x: 0, y: 0, width: 300, height: 100)
        
        print("Stepper Heigth: \(stepper.frame.height)")
        print("Stepper Widht: \(stepper.frame.width)")
    }
    
    @IBAction func stepperValueChanged(_ sender: Any) {
        let newValue = Int(stepper.value)
        model.setCurrentNumber(number: newValue)
    }
    
    func changeLabelText(withText: String) {
        DispatchQueue.main.async {
            self.labelText = withText
        }
    }
    
    func changeCurrentStepperValue(withValue: Double) {
        DispatchQueue.main.async {
            self.stepper.value = withValue
        }
    }
    
    func scheduledTimerWithTimeInterval() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateLabel), userInfo: nil, repeats: true)
    }
    
    @objc func updateLabel() {
        model.getCurrentNumber()
    }
}
