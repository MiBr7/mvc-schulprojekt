//
//  Model.swift
//  MVC School Project
//
//  Created by Tom Hüttmann on 27.11.17.
//  Copyright © 2017 Tom Hüttmann. All rights reserved.
//

import Foundation

protocol ModelDelegate: class {
    func changeLabelText(withText: String)
    func changeCurrentStepperValue(withValue: Double)
}

class Model {
    weak var delegate: ModelDelegate?
    
    func getCurrentNumber() {
        if let url = URL(string: "http://alphanudel.de:5001/number") {
            makeRequest(request: URLRequest(url: url)) { (response) in
                self.delegate?.changeLabelText(withText: response)
                if let currentValue = Double(response) {
                    self.delegate?.changeCurrentStepperValue(withValue: currentValue)
                }
            }
        }
    }
    
    func setCurrentNumber(number: Int) {
        if let url = URL(string: "http://alphanudel.de:5001/number?value=" + String(number)) {
            makeRequest(request: URLRequest(url: url)) { (response) in
                self.delegate?.changeLabelText(withText: response)
            }
        }
    }
    
    func makeRequest(request: URLRequest, completion: @escaping (String)->Void) {
        URLSession.shared.dataTask(with: request) {data, response, error in
            guard let data = data, error == nil else {
                print("error=\(String(describing: error))")
                return
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
            }
            
            let responseString = String(data: data, encoding: .utf8) ?? ""
            completion(responseString)
            }.resume()
    }
}
